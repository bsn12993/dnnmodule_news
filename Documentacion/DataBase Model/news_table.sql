
CREATE TABLE chk_News
( 
	id_news              int IDENTITY ( 1,1 ) ,
	title                varchar(40)  NULL ,
	shortDescription     varchar(250)  NULL ,
	lifeDate             datetime  NULL ,
	createdDate          datetime  NULL ,
	pathFile		     varchar(max) NULL,
	hasAttachment        int  NULL ,
	modifiedDate         datetime  NULL ,
	LongDescription      varchar(max)  NULL 
)
go



ALTER TABLE chk_News
	ADD CONSTRAINT XPKNews PRIMARY KEY  CLUSTERED (id_news ASC)
go


