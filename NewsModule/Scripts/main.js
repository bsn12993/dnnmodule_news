﻿
var URL = "/tasolnet/DesktopModules/NewsModule/API/NewsApi/";            //LOCAL

$(function () {
    GetNewsDetail(URL + "GetNewsById?id=" + idNews);
})

function SeeNews(id) {
    GetNewsDetail(URL + "GetNewsById?id=" + id);
}


function GetNewsDetail(url) {
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        beforeSend: function () {
        },
        success: function (response) {
            if (response.IsSuccess) {
                $("#title").html(response.Result.Title);
                var LongDescription = response.Result.LongDescription.replace(/\n/g, "<br/>");
                $("#longDescription").html(LongDescription);
                if (response.Result.HasFile)
                    $(".imageNews").html("<img src='" + response.Result.PathFile + "' width='100%' height='50px' class='img-responsive img-thumbnail'/>");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert("error");
        },
        complete: function () {
        }
    });
}
