﻿using Christoc.Modules.NewsModule.BD;
using Christoc.Modules.NewsModule.Models;
using Christoc.Modules.NewsModule.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsModule.DL
{
    public class DataAccess
    {
        private Response Response { get; set; }
        private string ConnectionString { get; set; }
        private ConnectionDB ConnectionDB = new ConnectionDB();
        private string Query_SP = string.Empty;
        private SqlParameter[] sqlParameters = null;

        public DataAccess()
        {
            Response = new Response();
            ConnectionString = ConnectionDB.GetConnectionLocalString(ConnectionDB.ServerTasolnetEnvionment);
        }

        public Response GetNews()
        {
            Query_SP = @"SELECT * FROM chk_News 
                        WHERE (CONVERT(DATE,DATEADD(d,0,lifeDate)))>=CONVERT(DATE,DATEADD(d,0,GETDATE())) 
                        ORDER BY createdDate desc";
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            List<News> lstNews = new List<News>();
                            News news = null;
                            while (reader.Read())
                            {
                                news = new News();
                                news.IdNews = (int)reader["id_news"];
                                news.Title = (reader["title"] == DBNull.Value) ? string.Empty : (string)reader["title"];
                                news.ShortDescription = (reader["shortDescription"] == DBNull.Value) ? string.Empty : (string)reader["shortDescription"];
                                news.LifeDate = (DateTime)reader["lifeDate"];
                                news.CreatedDate = (DateTime)reader["createdDate"];
                                news.HasFile = Convert.ToBoolean((int)reader["hasAttachment"]);
                                news.PathFile = (reader["pathFile"] == DBNull.Value) ? "" : (string)reader["pathFile"];
                                lstNews.Add(FileHelper.GetImage(news));
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se recuperarón las noticias",
                                Result = lstNews
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No se encontrarón noticias",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }


        public Response GetNewsById(int id)
        {
            Query_SP = @"SELECT * FROM chk_News 
                        WHERE id_news=@idNews";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@idNews",id)
            };
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            News news = null;
                            while (reader.Read())
                            {
                                news = new News();
                                news.IdNews = (int)reader["id_news"];
                                news.Title = (reader["title"] == DBNull.Value) ? string.Empty : (string)reader["title"];
                                news.ShortDescription = (reader["shortDescription"] == DBNull.Value) ? string.Empty : (string)reader["shortDescription"];
                                news.LongDescription = (reader["LongDescription"] == DBNull.Value) ? string.Empty : (string)reader["LongDescription"];
                                news.LifeDate = (DateTime)reader["lifeDate"];
                                news.HasFile = Convert.ToBoolean((int)reader["hasAttachment"]);
                                news.PathFile = (reader["pathFile"] == DBNull.Value) ? "" : (string)reader["pathFile"];
                                news = FileHelper.GetImage(news);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se recuperarón las noticias",
                                Result = news
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No se encontrarón noticias",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }

        public Response ValidateNewsTable(string nameTable)
        {
            Query_SP = @"SELECT * FROM INFORMATION_SCHEMA.TABLES
                        WHERE TABLE_TYPE = 'BASE TABLE'
                        AND TABLE_NAME = @nameTable";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@nameTable",nameTable)
            };
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Existe la tabla chk_News",
                                Result = null
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No se existe la tabla chk_News, instale el modulo NewsConfigurationModule",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
    }
}