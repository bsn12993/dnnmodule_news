﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsModule.Models
{
    public class News
    {
        public int IdNews { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public DateTime LifeDate { get; set; }
        public string PathFile { get; set; }
        public bool HasFile { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}