﻿using Christoc.Modules.NewsModule.DL;
using Christoc.Modules.NewsModule.Models;
using Christoc.Modules.NewsModule.Services;
using Christoc.Modules.NewsModule.Util;
using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Christoc.Modules.NewsModule.Controllers
{
    public class NewsController : DnnController
    {
        private DataService DataService = null;

        public NewsController()
        {
            DataService = new DataService();
        }
        // GET: News
        public ActionResult Index(int id = 0)
        {
            List<News> lstNews = new List<News>();
            var validarTable = DataService.ValidateNewsTable("chk_News");
            if (!validarTable.IsSuccess)
            {
                ViewBag.IsSuccess = validarTable.IsSuccess;
                ViewBag.Message = validarTable.Message;
                ViewBag.lstNews = lstNews;
                return View();
            }

            var response = DataService.GetNews();
            if (response.IsSuccess)
            {
                lstNews = (List<News>)response.Result;
            }
            ViewBag.lstNews = lstNews;
            ViewBag.IsSuccess = response.IsSuccess;
            ViewBag.Message = response.Message;
            ViewBag.Id = id;
            return View();
        }
    }
}