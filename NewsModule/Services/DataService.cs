﻿using Christoc.Modules.NewsModule.DL;
using Christoc.Modules.NewsModule.Models;
using Christoc.Modules.NewsModule.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsModule.Services
{
    public class DataService
    {
        private DataAccess DataAccess = null;

        public DataService()
        {
            DataAccess = new DataAccess();
        }

        public Response GetNews()
        {
            return DataAccess.GetNews();
        }

        public Response GetNewsById(int id)
        {
            var response = DataAccess.GetNewsById(id);
            if (response.IsSuccess)
            {
                var news = (News)response.Result;
                news.LongDescription.Replace("\r\n\r\n", "<br/>");
                response.Result = news;
                return response;
            }
            return DataAccess.GetNewsById(id);
        }

        public Response ValidateNewsTable(string table)
        {
            return DataAccess.ValidateNewsTable(table);
        }
    }
}