﻿using Christoc.Modules.NewsModule.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsModule.Util
{
    public class FileHelper
    {
        public static News GetImage(News news)
        {
            if (news.HasFile)
            {
                var file = news.PathFile.Split(new string[] { "Portals" }, StringSplitOptions.None);
                news.PathFile = string.Format("{0}/{1}{2}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority), "tasolnet/Portals", file[1]);
            }
            else
            {
                var root = HttpContext.Current.Server.MapPath("~");
                news.PathFile = string.Format("{0}{1}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority), "tasolnet/DesktopModules/MVC/NewsModule/Image/not-available.png");
            }
            return news;
        }
    }
}