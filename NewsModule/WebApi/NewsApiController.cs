﻿using Christoc.Modules.NewsModule.DL;
using Christoc.Modules.NewsModule.Models;
using Christoc.Modules.NewsModule.Services;
using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Christoc.Modules.NewsModule.WebApi
{
    public class NewsApiController : DnnApiController
    {
        private DataService DataService = null;
        private Response Response = null;

        public NewsApiController()
        {
            DataService = new DataService();
            Response = new Response();
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetMessage()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "HOLA", "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetNews()
        {
            var response = DataService.GetNews();
            if (response.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetNewsById([FromUri] int id)
        {
            var response = DataService.GetNewsById(id);
            if (response.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }
    }
}