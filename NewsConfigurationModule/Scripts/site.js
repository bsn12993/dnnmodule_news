﻿
var URL = "/DesktopModules/NewConfiguration/API/NewsConfigurationApi/"; //URL LOCAL/DEV

$("#div_alert_create").hide();
$("#div_alert_edit").hide();
$("#div_alert_delete").hide();

var obj = {
    Title: '',
    ShortDescription: '',
    LongDescription: '',
    LifeDate: '',
    HasFile: false,
    Attachment: {
        FileSize: '',
        FileName: '',
        FileExtencion: '',
        FileBase64: ''
    },
    TitleOld: '',
    PathFile: ''
};

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('jquery')) :
        typeof define === 'function' && define.amd ? define(['jquery'], factory) :
            (factory(global.jQuery));
}(this, (function ($) {
    'use strict';

    $.fn.datepicker.languages['es-ES'] = {
        format: 'dd/mm/yyyy',
        days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        weekStart: 1,
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
    };
    })));


$(document).ready(function () {

    ValidateLifeDate();

    //Se configura el formato del datepicker
    $('.datepicker').datepicker({
        language: 'es-ES',
        autoHide: true
    });

    $(".text-info").html("<b>El campo es sensible a saltos de línea</b>");

    $("#btn_createNews").click(function () {

        debugger;
        var dateNow = new Date();

        obj.Title = $("#title").val();
        obj.ShortDescription = $("#shortDescription").val();
        obj.LongDescription = $("#longDescription").val();
        obj.LifeDate = $("#lifeDate").val();

        if (obj.Title == "") {
            alert("El Titulo es requerido");
            return;
        }
        if (obj.ShortDescription == "") {
            alert("La Descripción Corta es requerido");
            return;
        }

        var ShortDescription = obj.ShortDescription.replace(/<[^>]*>?/g, ' ');
        var LengthShortDescription = parseInt(ShortDescription.length);
        if (LengthShortDescription >= 250) {
            alert("La longitud maxima de la Descripción Larga es 250");
            return;
        }
        obj.ShortDescription = ShortDescription;

        if (obj.LongDescription == "") {
            alert("La Descripción Larga es requerido");
            return;
        }

        var LongDescription = obj.LongDescription.replace(/<[^>]*>?/g, ' ');
        var LengthLongDescription = parseInt(LongDescription.length);
        if (LengthLongDescription >= 1500) {
            alert("La longitud maxima de la Descripción Larga es 1500");
            return;
        }
        obj.LongDescription = obj.LongDescription;

        if (obj.LifeDate == "") {
            alert("La Fecha de Vigencia es requerido");
            return;
        }

        var lifeDate = obj.LifeDate.split('/');
        var dateSelected = new Date(parseInt(lifeDate[2]), parseInt(lifeDate[1]) - 1, parseInt(lifeDate[0]));

        if (dateSelected <= dateNow) {
            alert("La Fecha de Vigencia debe ser mayor a la fecha actual");
            return;
        }
        PostNews(obj);
    });

    $("#btn_editNews").click(function () {

        debugger;
        var dateNow = new Date();

        obj.idNews = $("#idNews").val();
        obj.Title = $("#title").val();
        obj.TitleOld = $("#titleOld").val();
        obj.ShortDescription = $("#shortDescription").val();
        obj.LongDescription = $("#longDescription").val();
        obj.LifeDate = $("#lifeDate").val();
        obj.HasFile = (parseInt($("#hasFile").val()) == 1) ? true : false;
        if (obj.HasFile) {
            obj.PathFile = $("#pathFile").val();
        }

        if (obj.Title == "") {
            alert("El Titulo es requerido");
            return;
        }
        if (obj.ShortDescription == "") {
            alert("La Descripción Corta es requerido");
            return;
        }

        var ShortDescription = obj.ShortDescription.replace(/<[^>]*>?/g, ' ');
        var LengthShortDescription = parseInt(ShortDescription.length);
        if (LengthShortDescription >= 250) {
            alert("La longitud maxima de la Descripción Larga es 250");
            return;
        }
        obj.ShortDescription = ShortDescription;

        if (obj.LongDescription == "") {
            alert("La Descripción Larga es requerido");
            return;
        }

        var LongDescription = obj.LongDescription.replace(/<[^>]*>?/g, ' ');
        var LengthLongDescription = parseInt(LongDescription.length);
        if (LengthLongDescription >= 1500) {
            alert("La longitud maxima de la Descripción Larga es 1500");
            return;
        }
        obj.LongDescription = obj.LongDescription;

        if (obj.LifeDate == "") {
            alert("La Fecha de Vigencia es requerido");
            return;
        }

        var lifeDate = obj.LifeDate.split('/');
        var dateSelected = new Date(parseInt(lifeDate[2]), parseInt(lifeDate[1]) - 1, parseInt(lifeDate[0]));

        if (dateSelected <= dateNow) {
            alert("La Fecha de Vigencia debe ser mayor a la fecha actual");
            return;
        }
        PutNews(obj);
    });

    //Evento que valida que se haya cargado un archivo en el control
    $("#attached").change(function () {
        var inpute = document.getElementById("attached");
        var allowedExtensions = new Array();
        allowedExtensions.push("jpg");
        allowedExtensions.push("jpeg");
        allowedExtensions.push("png");
        if (inpute.files.length > 0) {
            var extencion = inpute.files[0].type;
            var name = inpute.files[0].name;
            var size = inpute.files[0].size;
            obj.Attachment.FileExtencion = extencion;
            obj.Attachment.FileName = name;
            obj.Attachment.FileSize = size;
            var ext = obj.Attachment.FileExtencion.split('/')[1];
            if (!allowedExtensions.contains(ext)) {
                alert("La extención del archivo no es permitida, solamente .jpeg/.jpg/.png");
                inpute.value = '';
                return;
            }
            var archivo = inpute.files[0];
            var lector = new FileReader();
            lector.addEventListener(
                "load",
                function (evento) {
                    document.getElementById("contentFile").value = evento.target.result;
                    console.log(evento.target.result);
                    obj.Attachment.FileBase64 = document.getElementById("contentFile").value;
                    obj.HasFile = true;
                }, false
            );
            lector.readAsDataURL(archivo);
        }
    });
});

function SelectNewsById(idNews) {
    RequestGetNewsById(URL + "GetNewsById?idNews=" + idNews);
}

function DeleteNewsById(idNews) {
    RequestDeleteNews(URL + "DeleteNews?idNews=" + idNews);
}

function PutNews(obj) {
    RequestPutNews(URL + "EditNews", obj);
}

function PostNews(obj) {
    RequestPostNews(URL + "CreateNews", obj);
}

function LoadTable(json) {
    $('#tbl_News').DataTable({
        responsive: true,
        scrollCollapse: true,
        scrollY: '50h',
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "No se encontrarón registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se encontrarón registros",
            "infoFiltered": "(Filtrando de _MAX_ regístros)",
            "infoPostFix": "",
            "thousands": ",",
            "loadingRecords": "Cargando",
            "processing": "Procesando",
            "search": "Buscar",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
}


 
function ClearFields() {
    $("#title").val("");
    $("#lifeDate").val("");
    $("#shortDescription").val("");
    $('#shortDescription').data("wysihtml5").editor.clear();
    $("#longDescription").val("");
    $('#longDescription').data("wysihtml5").editor.clear();
    if (document.getElementById("attached").files.length > 0) {
        var control = jQuery('#attached');
        control.replaceWith(control = control.val('').clone(true));
    } 
}

function SetDataIntoField(obj) {
    var json = JSON.parse(obj);
    $("#idNews").val(json.idNews);
    $("#title").val(json.Title);
    $("#lifeDate").val(json.LifeDate);
    $("#shortDescription").val(json.ShortDescription);
    $("#longDescription").val(json.LongDescription);
}

function RequestGetNews(url) {
    $.ajax({
        url: url,
        type:"GET",
        success:function(data){
            alert("second success");
            if (data.IsSuccess) LoadTable(data.Result);
            else alert(data.Message);
        },
        error:function(x,y,z){
            console.log("error: " + x + "-" + y + "-" + z);
        },
        complete:function(){

        }
    });
}


function RequestGetNewsById(url) {
    return false;
}

function RequestPostNews(url, obj) {
    $.ajax({
        url: url,
        type:"POST",
        data:obj,
        success: function (data) {
            if (data.IsSuccess) {
                $("#div_alert_create").show();
                $("#txt_mensaje_create").html(data.Message);
                $("#div_alert_create").removeClass("alert-info").addClass("alert-success");
                $("#icon_create").removeClass("fa fa-info-circle").addClass("fa fa-check-circle");
                ClearFields();
                $(".text-info").html("");
                alert(data.Message);
            }
            else {
                $("#div_alert_create").show();
                $("#txt_mensaje_create").html(data.Message);
                $("#div_alert_create").removeClass("alert-info").addClass("alert-danger");
                $("#icon_create").removeClass("fa fa-info-circle").addClass("fa fa-times");
                alert(data.Message);
            }
        },
        error:function(x,y,z){
            console.log("error: " + x + "-" + y + "-" + z);
            alert("error: " + x + "-" + y + "-" + z);
        },
        complete:function(){

        }
    });
}

function RequestPutNews(url, obj) {
    $.ajax({
        url: url,
        type:"POST",
        data:obj,
        success:function(data){
            if (data.IsSuccess) {
                $("#div_alert_edit").show();
                $("#txt_mensaje_edit").html(data.Message);
                $("#div_alert_edit").removeClass("alert-info").addClass("alert-success");
                $("#icon_edit").removeClass("fa fa-info-circle").addClass("fa fa-check-circle");
                $(".text-info").html("");
                ClearFields();
                alert(data.Message);
                setTimeout(function () {
                    window.location.href = urlIndex;
                }, 2000);
            }
            else {
                $("#div_alert_edit").show();
                $("#txt_mensaje_edit").html(data.Message);
                $("#div_alert_edit").removeClass("alert-info").addClass("alert-danger");
                $("#icon_edit").removeClass("fa fa-info-circle").addClass("fa fa-times");
                alert(data.Message);
            }
        },
        error:function(x,y,z){
            console.log("error: " + x + "-" + y + "-" + z);
            alert("error: " + x + "-" + y + "-" + z);
        },
        complete:function(){

        }
    });
}

function RequestDeleteNews(url) {
    $.ajax({
        url: url,
        type:"GET",
        success:function(data){
            if (data.IsSuccess) {
                $("#div_alert_delete").show();
                $("#txt_mensaje_delete").html(data.Message);
                $("#div_alert_delete").removeClass("alert-info").addClass("alert-success");
                $("#icon_delete").removeClass("fa fa-info-circle").addClass("fa fa-check-circle");
                alert(data.Message);
                setTimeout(function () {
                    window.location.reload();
                },2000)
            }
            else {
                $("#div_alert_delete").show();
                $("#txt_mensaje_delete").html(data.Message);
                $("#div_alert_delete").removeClass("alert-info").addClass("alert-danger");
                $("#icon_delete").removeClass("fa fa-info-circle").addClass("fa fa-times");
            }
        },
        error:function(x,y,z){
            console.log("error: " + x + "-" + y + "-" + z);
            alert("error: " + x + "-" + y + "-" + z);
        },
        complete:function(){

        }
    });
}


function ValidateLifeDate() {
    $(".lifeDate").each(function (k, v) {
        console.log(v);
        var date = v.innerHTML;
        var d = date.split('/');
        var getDate = new Date(parseInt(d[2]), parseInt(d[1]) - 1, parseInt(d[0]));
        getDate.setHours(0, 0, 0, 0);
        var getDateNow = new Date();
        getDateNow.setHours(0, 0, 0, 0);

        if (getDate > getDateNow) {
            //$(this).addClass("text-danger").addClass("bg-danger");
        }
        else {
            $(this).addClass("text-danger").addClass("bg-danger");
        }

    });
}


Array.prototype.contains = function (needle) {
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}

 