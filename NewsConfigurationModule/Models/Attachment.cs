﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.Models
{
    public class Attachment
    {
        public string FileSize { get; set; }
        public string FileName { get; set; }
        public string FileExtencion { get; set; }
        public string FileBase64 { get; set; }
    }
}