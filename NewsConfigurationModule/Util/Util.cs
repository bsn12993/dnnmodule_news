﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.Util
{
    public class Util
    {
        /// <summary>
        /// Metodo que convierte una fecha string a DateTime
        /// </summary>
        /// <param name="str">fecha en string</param>
        /// <returns></returns>
        public static DateTime ConvertStringToDateTime(string str)
        {
            var d = str.Split('/');
            return new DateTime(Convert.ToInt32(d[2]), Convert.ToInt32(d[1]), Convert.ToInt32(d[0]));
        }
    }
}