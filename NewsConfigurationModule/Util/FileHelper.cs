﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.Util
{
    public class FileHelper
    {
        /// <summary>
        /// Metodo que guarda el archivo seleccionado en una ubicación fisica
        /// </summary>
        /// <param name="base64">archivo en base</param>
        /// <param name="nameFile">nombre del archivo original</param>
        public static string SaveFile(string base64, string nameFile)
        {
            var file = base64.Split(',')[1];
            byte[] imageBytes = Convert.FromBase64String(file);
            var root = HttpContext.Current.Server.MapPath("~");
            var folder = CreateFolder(Path.Combine(root, "Portals/0/ImagesNews"));
            var path = Path.Combine(folder, nameFile);
            File.WriteAllBytes(path, imageBytes);
            return path;
        }

        /// <summary>
        /// Metodo que valida la extención del archivo seleccionado
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool ValidateExtension(string fileExtension, string file)
        {
            var lstFileExtensions = fileExtension.Split(',');
            if (lstFileExtensions.Contains(file.Split('/')[1])) return true;
            else return false;
        }

        /// <summary>
        /// Metodo que valida si el directorio existe.
        /// en caso de no existir se crea
        /// en caso de exister se retorna la ruta
        /// </summary>
        /// <param name="path">ruta a validar</param>
        /// <param name="idmodule">id del modulo actual</param>
        /// <returns>retorna la ruta de ubicacion</returns>
        public static string CreateFolder(string path)
        {
            if (Directory.Exists(path))
            {
                return path;
            }
            Directory.CreateDirectory(path);
            return path;
        }

        /// <summary>
        /// Metodo que renombra un archivo existente
        /// </summary>
        /// <param name="path">url fisica del archivo</param>
        /// <param name="newTitle">nuevo titulo</param>
        /// <param name="oldTitle">titulo actual</param>
        /// <returns></returns>
        public static string RenameFile(string path, string newTitle, string oldTitle)
        {
            if (File.Exists(path))
            {
                var root = HttpContext.Current.Server.MapPath("~");
                var oldPath = Path.Combine(root, "Portals/0/ImagesNews", oldTitle);
                var newPath = Path.Combine(root, "Portals/0/ImagesNews", newTitle);
                File.Move(oldPath, newPath);
                return newPath;
            }
            return string.Empty;
        }

        /// <summary>
        /// Metodo que elimina un archivo
        /// </summary>
        /// <param name="path">url fisica del archivo</param>
        public static void DeleteFile(string path)
        {
            var root = HttpContext.Current.Server.MapPath("~");
            var pathfile = path.Split(new string[] { "Portals" }, StringSplitOptions.None);
            var file = (pathfile[1]).ToString();
            var url = root + "Portals/" + file;
            if (File.Exists(url))
            {   
                File.Delete(url);
            }
        }

       
         
    }
}