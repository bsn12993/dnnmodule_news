﻿using Christoc.Modules.NewsConfigurationModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.Util
{
    public class ValidateHelper
    {
        /// <summary>
        /// Metodo que valida los datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static object[] ValidateFields(News model)
        {
            if (string.IsNullOrEmpty(model.Title))
            {
                return new object[2]
                {
                    false,
                    "El campo Titulo es requerido."
                };
            }
            if (string.IsNullOrEmpty(model.ShortDescription))
            {
                return new object[2]
                {
                    false,
                    "El campo Descripción Corta es requerido"
                };
            }

            if (!ValidateDescriptionLength(model.ShortDescription,250))
            {
                return new object[2]
                {
                    false,
                    "La cantidad de carácteres del campo Descripción Corta debe ser solamente 250"
                };
            }

            if (string.IsNullOrEmpty(model.LongDescription))
            {
                return new object[2]
                {
                    false,
                    "El campo Descripción Larga es requerido"
                };
            }

            if (!ValidateDescriptionLength(model.LongDescription, 1500))
            {
                return new object[2]
                {
                    false,
                    "La cantidad de carácteres del campo Descripción Larga debe ser solamente 1500"
                };
            }

            if (model.LifeDate == null) 
            {
                return new object[2]
                {
                    false,
                    "El campo Fecha de Vigencia es requerido"
                };
            }

            var lifeDate = Util.ConvertStringToDateTime(model.LifeDate);
            var compareDate = DateTime.Compare(DateTime.Now.Date, lifeDate);
            if (compareDate == 0 || compareDate > 0) 
            {
                return new object[2]
                {
                    false,
                    "El campo Fecha de Vigencia debe ser mayor a la fecha actual"
                };
            }

            if (model.HasFile)
            {
                if (model.Attachment == null)
                {
                    return new object[2]
                    {
                        false,
                        "No se cargo el archivo correctamente"
                    };
                }

                if (string.IsNullOrEmpty(model.Attachment.FileBase64))
                {
                    return new object[2]
                    {
                        false,
                        ""
                    };
                }

                if (string.IsNullOrEmpty(model.Attachment.FileExtencion))
                {
                    return new object[2]
                    {
                        false,
                        ""
                    };
                }
                if (string.IsNullOrEmpty(model.Attachment.FileName))
                {
                    return new object[2]
                    {
                        false,
                        ""
                    };
                }
            }

            return new object[2]
            {
                true,
                "Campos correctos"
            };
        }

        public static bool ValidateDescriptionLength(string text, int size)
        {
            string str = Regex.Replace(text, "<.*?>", string.Empty);
            var length = str.Length;
            if (length <= size) return true;
            else return false;
        }
    }
}