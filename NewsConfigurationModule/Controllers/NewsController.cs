﻿using Christoc.Modules.NewsConfigurationModule.Models;
using Christoc.Modules.NewsConfigurationModule.Services;
using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Christoc.Modules.NewsConfigurationModule.Controllers
{
    public class NewsController : DnnController
    {
        private DataService DataService = null;
        Response Response = null;

        public NewsController()
        {
            DataService = new DataService();
            Response = new Response();
        }
        // GET: News
        public ActionResult Index()
        {
            List<News> lstNews = new List<News>();
            bool HasData = false;
            string Message = string.Empty;
            var response = DataService.GetNews();
            if (response.IsSuccess)
            {
                lstNews = (List<News>)response.Result;
                HasData = response.IsSuccess;
                Message = "";
            }
            else
            {
                lstNews = null;
                HasData = response.IsSuccess;
                Message = response.Message;
            }

            ViewBag.lstNews = lstNews;
            ViewBag.HasData = HasData;
            ViewBag.Message = Message;

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            News News = new News();
            bool HasData = false;
            string Message = string.Empty;
            var response = DataService.GetNewsById(id);
            if (response.IsSuccess)
            {
                News = (News)response.Result;
                HasData = response.IsSuccess;
                Message = "";
            }
            else
            {
                News = null;
                HasData = response.IsSuccess;
                Message = response.Message;
            }

            ViewBag.News = News;
            ViewBag.HasData = HasData;
            ViewBag.Message = Message;
            return View();
        }
    }
}