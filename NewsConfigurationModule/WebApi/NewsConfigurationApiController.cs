﻿using Christoc.Modules.NewsConfigurationModule.Models;
using Christoc.Modules.NewsConfigurationModule.Services;
using Christoc.Modules.NewsConfigurationModule.Util;
using DotNetNuke.Web.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Christoc.Modules.NewsConfigurationModule.WebApi
{
    public class NewsConfigurationApiController : DnnApiController
    {
        private Response Response { get; set; }
        private DataService DataService = null;

        public NewsConfigurationApiController()
        {
            Response = new Response();
            DataService = new DataService();
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetMessage()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Hello", "application/json");
        }

        /// <summary>
        /// Metodo que crea una noticia
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage CreateNews([FromBody] News model)
        {
            if (!Convert.ToBoolean(ValidateHelper.ValidateFields(model)[0])) 
            {
                Response.IsSuccess = Convert.ToBoolean(ValidateHelper.ValidateFields(model)[0]);
                Response.Message = Convert.ToString(ValidateHelper.ValidateFields(model)[1]);
                return Request.CreateResponse(HttpStatusCode.Accepted, "", "application/json");
            }
            try
            {
                var validate = DataService.GetNewsByTitle(model.Title);
                if (!validate.IsSuccess)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, validate, "application/json");
                }
                var response = DataService.PostNews(model);
                if (response.IsSuccess)
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
                else
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
            }
            catch(Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        /// <summary>
        /// Metodo que consulta las noticias
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetNews()
        {
            try
            {
                var response = DataService.GetNews();
                if (response.IsSuccess)
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                    var json = JsonConvert.SerializeObject(response.Result);
                    Response.Result = json;
                }
                else
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        /// <summary>
        /// Metodo que consulta por id
        /// </summary>
        /// <param name="idNews"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetNewsById([FromUri]int idNews)
        {
            try
            {
                var response = DataService.GetNewsById(idNews);
                if (response.IsSuccess)
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                    var json = JsonConvert.SerializeObject(response.Result);
                    Response.Result = json;
                }
                else
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        /// <summary>
        /// Metodo que edita la notica
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage EditNews([FromBody] News model)
        {
            //if (!Convert.ToBoolean(ValidateHelper.ValidateFields(model)[0]))
            //{
            //    Response.IsSuccess = Convert.ToBoolean(ValidateHelper.ValidateFields(model)[0]);
            //    Response.Message = Convert.ToString(ValidateHelper.ValidateFields(model)[1]);
            //    return Request.CreateResponse(HttpStatusCode.Accepted, "", "application/json");
            //}
            try
            {
                var validate = DataService.GetNewsByTitleAndId(model.Title, model.IdNews);
                if (!validate.IsSuccess)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, validate, "application/json");
                }
                var response = DataService.PutNews(model.IdNews, model);
                if (response.IsSuccess)
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
                else
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        /// <summary>
        /// Metodo que elimina una noticia
        /// </summary>
        /// <param name="idNews"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage DeleteNews([FromUri] int idNews)
        {
            try
            {
                var getNews = DataService.GetNewsById(idNews);
                if (!getNews.IsSuccess)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, getNews, "application/json");
                }
                News news = (News)getNews.Result;
                var response = DataService.DeleteNews(idNews, news);
                if (response.IsSuccess)
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
                else
                {
                    Response.IsSuccess = response.IsSuccess;
                    Response.Message = response.Message;
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }
    }
}