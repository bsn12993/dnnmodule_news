﻿using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.WebApi
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("NewConfiguration", "default", "{controller}/{action}",
                new[] { "Christoc.Modules.NewsConfigurationModule.WebApi" });
        }
    }
}