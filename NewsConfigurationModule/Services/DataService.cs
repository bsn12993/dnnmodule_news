﻿using Christoc.Modules.NewsConfigurationModule.DAL;
using Christoc.Modules.NewsConfigurationModule.Models;
using Christoc.Modules.NewsConfigurationModule.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.Services
{
    public class DataService
    {
        DataAccess DataAccess = null;

        public DataService()
        {
            DataAccess = new DataAccess();
        }

        /// <summary>
        /// Metodo que consulta las noticias
        /// </summary>
        /// <returns></returns>
        public Response GetNews()
        {
            return DataAccess.GetNews();
        }
        /// <summary>
        /// Metodo que consulta la noticia en base al id
        /// </summary>
        /// <param name="id">identificador de la noticia</param>
        /// <returns></returns>
        public Response GetNewsById(int id)
        {
            var response = DataAccess.GetNewsById(id);
            if (response.IsSuccess)
            {
                var news = (News)response.Result;
                if (news.HasFile)
                {
                    var file = news.PathFile.Split(new string[] { "Portals" }, StringSplitOptions.None);
                    news.PathFile = string.Format("{0}/{1}{2}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority), "tasinet2/Portals", file[1]);   
                }
                response.Result = news;
            }
            return response;
        }
        /// <summary>
        /// Metodo que registra una noticia 
        /// </summary>
        /// <param name="news">objeto de la noticia</param>
        /// <returns></returns>
        public Response PostNews(News news)
        {
            if (!string.IsNullOrEmpty(news.Attachment.FileBase64))
            {
                var saveImage = FileHelper.SaveFile(news.Attachment.FileBase64, string.Format("{0}.{1}", news.Title, news.Attachment.FileExtencion.Split('/')[1]));
                if (string.IsNullOrEmpty(saveImage))
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "No se pudo guardar la imagen"
                    };
                }
                news.PathFile = saveImage;
            }
            return DataAccess.PostNews(news);
        }
        /// <summary>
        /// Metodo que edita una noticia
        /// </summary>
        /// <param name="id"></param>
        /// <param name="news"></param>
        /// <returns></returns>
        public Response PutNews(int id, News news)
        {
            var root = HttpContext.Current.Server.MapPath("~");
            if (!string.IsNullOrEmpty(news.Attachment.FileBase64))
            {    
                if (news.HasFile)
                {
                    var phisicalPath = news.PathFile.Split(new string[] { "Portals" }, StringSplitOptions.None);
                    news.PathFile = string.Format("{0}{1}{2}", root, "Portals", phisicalPath[1]);
                    FileHelper.DeleteFile(news.PathFile);
                }
                var saveImage = FileHelper.SaveFile(news.Attachment.FileBase64, string.Format("{0}.{1}", news.Title, news.Attachment.FileExtencion.Split('/')[1]));
                if (!string.IsNullOrEmpty(saveImage))
                {
                    news.PathFile = saveImage;
                    news.HasFile = true;
                }
            }
            else if (!news.Title.ToLower().Equals(news.TitleOld.ToLower()))
            {
                var file = news.PathFile.Split(new string[] { "Portals" }, StringSplitOptions.None);
                var extencion = (file[1]).Split('.')[1];
                var newPath = FileHelper.RenameFile(Path.Combine(root,
                    string.Format("{0}/{1}", "Portals", file[1])),
                    string.Format("{0}.{1}", news.Title, extencion),
                    string.Format("{0}.{1}", news.TitleOld, extencion));
                news.PathFile = newPath;
            }
            else
            {
                if (!string.IsNullOrEmpty(news.PathFile))
                {
                    var phisicalPath = news.PathFile.Split(new string[] { "Portals" }, StringSplitOptions.None);
                    news.PathFile = string.Format("{0}{1}{2}", root, "Portals", phisicalPath[1]);
                }
            }
            return DataAccess.PutNews(id, news);
        }
        /// <summary>
        /// Metodo que elimina una noticia
        /// </summary>
        /// <param name="id"></param>
        /// <param name="news"></param>
        /// <returns></returns>
        public Response DeleteNews(int id, News news)
        {
            if (news.HasFile) FileHelper.DeleteFile(news.PathFile);
            return DataAccess.DeleteNews(id);
        }
        /// <summary>
        /// Metodo consulta una noticia por titulo
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public Response GetNewsByTitle(string title)
        {
            return DataAccess.GetNewsByTitle(title);
        }
        /// <summary>
        /// Metodo que consulta una noticia por titulo por id
        /// </summary>
        /// <param name="title"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response GetNewsByTitleAndId(string title, int id)
        {
            return DataAccess.GetNewsByTitleAndId(title, id);
        }
    }
}