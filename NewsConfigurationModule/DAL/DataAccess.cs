﻿using Christoc.Modules.NewsConfigurationModule.BD;
using Christoc.Modules.NewsConfigurationModule.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsConfigurationModule.DAL
{
    public class DataAccess
    {
        private Response Response { get; set; }
        private string ConnectionString { get; set; }
        private ConnectionDB ConnectionDB = new ConnectionDB();
        private string Query_SP = string.Empty;
        private SqlParameter[] sqlParameters = null;

        public DataAccess()
        {
            Response = new Response();
            ConnectionString = ConnectionDB.GetConnectionLocalString(ConnectionDB.ServerEnvionment);
        }
        /// <summary>
        /// Metodo que consulta las noticias
        /// </summary>
        /// <returns></returns>
        public Response GetNews()
        {
            Query_SP = "SELECT * FROM chk_News";
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            List<News> lstNews = new List<News>();
                            News news = null;
                            while (reader.Read())
                            {
                                news = new News();
                                news.IdNews = (int)reader["id_news"];
                                news.Title = (reader["title"] == DBNull.Value) ? string.Empty : (string)reader["title"];
                                news.ShortDescription = (reader["shortDescription"] == DBNull.Value) ? string.Empty : (string)reader["shortDescription"];
                                news.LifeDate = ((DateTime)reader["lifeDate"]).ToString("dd/MM/yyyy");
                                news.CreatedDate = (DateTime)reader["createdDate"];
                                news.HasFile = Convert.ToBoolean((int)reader["hasAttachment"]);
                                if (Convert.ToDateTime(news.LifeDate) >= DateTime.Now.Date)
                                    news.Status = "Vigente";
                                else news.Status = "Caducado";
                                
                                lstNews.Add(news);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se recuperarón las noticias",
                                Result = lstNews,
                                IsList = true
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No se encontrarón noticias",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
        /// <summary>
        /// Metodo que consulta la noticia por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response GetNewsById(int id)
        {
            Query_SP = "SELECT * FROM chk_News WHERE id_news=@idNews";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@idNews",id)
            };

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            News news = null;
                            while (reader.Read())
                            {
                                news = new News();
                                news.IdNews = (int)reader["id_news"];
                                news.Title = (reader["title"] == DBNull.Value) ? string.Empty : (string)reader["title"];
                                news.ShortDescription = (reader["shortDescription"] == DBNull.Value) ? string.Empty : (string)reader["shortDescription"];
                                news.LongDescription = (reader["LongDescription"] == DBNull.Value) ? string.Empty : (string)reader["LongDescription"];
                                news.LifeDate = ((DateTime)reader["lifeDate"]).ToString("dd/MM/yyyy");
                                news.CreatedDate = (DateTime)reader["createdDate"];
                                news.HasFile = Convert.ToBoolean((int)reader["hasAttachment"]);
                                if (news.HasFile)
                                {
                                    news.PathFile = (reader["pathFile"] == DBNull.Value) ? string.Empty : (string)reader["pathFile"];
                                }
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se recuperarón las noticias",
                                Result = news,
                                IsList = false
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No se encontrarón noticias",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// Metodo que consulta una noticia por titulo
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public Response GetNewsByTitle(string title)
        {
            Query_SP = "SELECT * FROM chk_News WHERE title=@title";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@title",title)
            };

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "Ya se encuentra registrada una noticia con el mismo titulo",
                                Result = null,
                                IsList = false
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "No existe aún una noticia con el mismo titulo",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
        /// <summary>
        /// Metodo que consulta la noticia por titulo y por id
        /// </summary>
        /// <param name="title"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response GetNewsByTitleAndId(string title, int id)
        {
            Query_SP = "SELECT * FROM chk_News WHERE title=@title and id_news<>@idNews";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@title",title),
                new SqlParameter("@idNews",id)
            };

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "Ya se encuentra registrada una noticia con el mismo titulo",
                                Result = null,
                                IsList = false
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "No existe aún una noticia con el mismo titulo",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
        /// <summary>
        /// Metodo que registra una noticia
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public Response PostNews(News news)
        {
            Query_SP = @"INSERT INTO 
                            chk_News(title,shortDescription,LongDescription,lifeDate,hasAttachment,createdDate,pathFile)
                        VALUES(@title, @shortDescription, @LongDescription, @lifeDate, @hasAttachment, getdate(),@pathFile)";
            object pathFile = null;
            if (string.IsNullOrEmpty(news.PathFile)) pathFile = DBNull.Value;
            else pathFile = news.PathFile;

            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@title",news.Title),
                new SqlParameter("@shortDescription",news.ShortDescription),
                new SqlParameter("@LongDescription",news.LongDescription),
                new SqlParameter("@lifeDate",new DateTime(Convert.ToInt32(news.LifeDate.Split('/')[2]),Convert.ToInt32(news.LifeDate.Split('/')[1]),Convert.ToInt32(news.LifeDate.Split('/')[0]))),
                new SqlParameter("@hasAttachment",news.HasFile),
                new SqlParameter("@pathFile",pathFile)
            };
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var insert = cmd.ExecuteNonQuery();
                        if (insert > 0)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se ha creado la noticia"
                            };
                        }
                        else
                            throw new Exception("No se pudo crear la noticia");
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
        /// <summary>
        /// Metodo que edita una noticia
        /// </summary>
        /// <param name="id"></param>
        /// <param name="news"></param>
        /// <returns></returns>
        public Response PutNews(int id, News news)
        {
            Query_SP = @"UPDATE chk_News SET 
                        title = @title, 
	                    shortDescription = @shortDescription, 
	                    LongDescription = @LongDescription,
	                    lifeDate = @lifeDate,
	                    hasAttachment = @hasAttachment,
                        pathFile = @pathFile,
	                    modifiedDate = GETDATE()
                        WHERE id_news = @idNews";
            object pathFile = null;
            if (string.IsNullOrEmpty(news.PathFile)) pathFile = DBNull.Value;
            else pathFile = news.PathFile;

            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@idNews",id),
                new SqlParameter("@title",news.Title),
                new SqlParameter("@shortDescription",news.ShortDescription),
                new SqlParameter("@LongDescription",news.LongDescription),
                new SqlParameter("@lifeDate",new DateTime(Convert.ToInt32(news.LifeDate.Split('/')[2]),Convert.ToInt32(news.LifeDate.Split('/')[1]),Convert.ToInt32(news.LifeDate.Split('/')[0]))),
                new SqlParameter("@hasAttachment",news.HasFile),
                new SqlParameter("@pathFile",pathFile)
            };

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var insert = cmd.ExecuteNonQuery();
                        if (insert > 0)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se ha editado la noticia"
                            };
                        }
                        else
                            throw new Exception("No se pudo editar la noticia");
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
        /// <summary>
        /// Metodo que elimina una noticia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response DeleteNews(int id)
        {
            Query_SP = "DELETE FROM chk_News WHERE id_news=@idNews";
            sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@idNews",id)
            };

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(Query_SP, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameters);
                        connection.Open();
                        var insert = cmd.ExecuteNonQuery();
                        if (insert > 0)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se ha eliminado la noticia"
                            };
                        }
                        else
                            throw new Exception("No se pudo eliminar la noticia");
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
    }
}