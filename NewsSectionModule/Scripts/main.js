﻿var idModule = 0;
var URL = "/tasolnet/DesktopModules/NewsSectionModule/API/NewsSectionWebApi/GetModuleID";

var fullUrl = "";
var url_part1 = "/tasolnet/Noticias/moduleId/";
var url_part2 = "/controller/News/action/Index";

$(function () {
    GetModuleID();
})

function GetModuleID() {
    $.ajax({
        url: URL,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        beforeSend: function () {
        },
        success: function (response) {
            fullUrl = url_part1 + "" + response + "" + url_part2;
            //alert("url: " + fullUrl);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert("error");
        },
        complete: function () {
        }
    });
}

function GoToNews(id) {
    window.location.href = fullUrl + "?id=" + id;
}