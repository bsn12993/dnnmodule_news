﻿using Christoc.Modules.NewsSectionModule.DL;
using Christoc.Modules.NewsSectionModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsSectionModule.Service
{
    public class DataService
    {
        private DataAccess DataAccess = null;

        public DataService()
        {
            DataAccess = new DataAccess();
        }

        public Response GetNews()
        {
            return DataAccess.GetNews();
        }

        public Response ValidateNewsTable(string table)
        {
            return DataAccess.ValidateNewsTable(table);
        }

        public static int GetModuleID()
        {
            int moduleID = 0;
            var ModuleInfo = DotNetNuke.Entities.Modules.ModuleController.Instance.GetAllModules();
            foreach (var mod in ModuleInfo)
            {
                var moduleInfo = (DotNetNuke.Entities.Modules.ModuleInfo)mod;
                if (moduleInfo.ModuleTitle.Equals("Noticias"))
                {
                    moduleID = moduleInfo.ModuleID;
                    break;
                }
            }
            return moduleID;
        }
    }
}