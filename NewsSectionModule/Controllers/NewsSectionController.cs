﻿using Christoc.Modules.NewsSectionModule.Models;
using Christoc.Modules.NewsSectionModule.Service;
using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Christoc.Modules.NewsSectionModule.Controllers
{
    public class NewsSectionController : DnnController
    {
        private DataService DataService = null;

        public NewsSectionController()
        {
            DataService = new DataService();
        }

        // GET: NewsSection
        public ActionResult Index(int id = 0)
        {
            List<News> lstNews = new List<News>();
            var validarTable = DataService.ValidateNewsTable("chk_News");
            if (!validarTable.IsSuccess)
            {
                ViewBag.IsSuccess = validarTable.IsSuccess;
                ViewBag.Message = validarTable.Message;
                ViewBag.lstNews = lstNews;
                return View();
            }

            var response = DataService.GetNews();
            if (response.IsSuccess)
            {
                lstNews = (List<News>)response.Result;
            }
            ViewBag.lstNews = lstNews;
            ViewBag.IsSuccess = response.IsSuccess;
            ViewBag.Message = response.Message;
            return View();
        }
    }
}