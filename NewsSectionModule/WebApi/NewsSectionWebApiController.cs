﻿using Christoc.Modules.NewsSectionModule.Models;
using Christoc.Modules.NewsSectionModule.Service;
using DotNetNuke.Web.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Christoc.Modules.NewsSectionModule.WebApi
{
    public class NewsSectionWebApiController : DnnApiController
    {
        private DataService DataService = null;
        private Response Response = null;

        public NewsSectionWebApiController()
        {
            DataService = new DataService();
            Response = new Response();
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetMessage()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "HOLA", "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetNews()
        {
            var response = DataService.GetNews();
            if (response.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetModuleID()
        {
            var idModule = DataService.GetModuleID();
            return Request.CreateResponse(HttpStatusCode.OK, idModule, "application/json");
        }
    }
}