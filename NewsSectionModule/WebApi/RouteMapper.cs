﻿using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.NewsSectionModule.WebApi
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("NewsSectionModule", "default", "{controller}/{action}",
                new[] { "Christoc.Modules.NewsSectionModule.WebApi" });
        }
    }
}